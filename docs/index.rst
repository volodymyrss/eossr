.. eossr documentation master file, created by
   sphinx-quickstart on Wed Sep  8 16:24:23 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. mdinclude:: README.md


Documentation table of contents
===============================

.. toctree::
   :maxdepth: 3
   :glob:

   README
   docstring
   metadata
   notebooks/ossr_statistics.ipynb
   examples
   codemeta_to_zenodo
   gitlab_to_zenodo
   snippets
   resources


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
