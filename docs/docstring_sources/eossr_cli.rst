

eOSSR CLI programs
------------------

.. toctree::
   :maxdepth: 1

   eossr_cli/eossr-codemeta2zenodo
   eossr_cli/eossr-upload-repository
   eossr_cli/eossr-metadata-validator
   eossr_cli/eossr-zenodo-validator
   eossr_cli/eossr-zip-repository
   eossr_cli/eossr-check-connection-zenodo
