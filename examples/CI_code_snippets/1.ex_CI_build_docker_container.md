# Build a Docker container during the CI process

 - Builds a Docker container during the CI process.
 - Uploads the container to the GitLab container registry.

**NOTE**. You should provide the Docker recipe (`Dockerfile`) in the root directory of your project.

```yaml
stages:
  - build_container


build_docker:
  stage: build_container
  image: docker:19.03.12
  services:
    - docker:19.03.12-dind
  before_script:
    - cat /etc/os-release  # "Alpine Linux v3.12"
    - apk add git
    - export LAST_RELEASE=`git ls-remote --tags --refs --sort="v:refname" $CI_PROJECT_URL.git | tail -n1 | sed 's/.*\///'`
    - if [ -z "$LAST_RELEASE" ]; then export LAST_RELEASE="main"; fi;
    - echo $LAST_RELEASE
  script:
    - echo "$CI_REGISTRY_PASSWORD" | docker login -u "$CI_REGISTRY_USER" "$CI_REGISTRY" --password-stdin
    - docker build -t $CI_REGISTRY_IMAGE:$LAST_RELEASE .
    - docker push $CI_REGISTRY_IMAGE:$LAST_RELEASE
  only:
    - tags
```
